/**
 * 
 */
package com.apkirai.emailbounce.config;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * @author tokase
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = { "com.apkirai.emailbounce" })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
