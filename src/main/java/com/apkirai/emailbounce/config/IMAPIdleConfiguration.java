package com.apkirai.emailbounce.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.mail.ImapIdleChannelAdapter;
import org.springframework.integration.mail.ImapMailReceiver;

@Configuration
public class IMAPIdleConfiguration {

	@Value("${imap.mailReceiverURL}")
	private String imapMailReceiverURL;

	private Properties javaMailProperties() {
		Properties javaMailProperties = new Properties();
		/*
		 * <prop
		 * key="mail.imap.socketFactory.class">javax.net.ssl.SSLSocketFactory</
		 * prop> <prop key="mail.imap.socketFactory.fallback">false</prop> <prop
		 * key="mail.store.protocol">imaps</prop> <prop
		 * key="mail.debug">false</prop> <prop
		 * key="mail.smtp.timeout">10000</prop>
		 */
		javaMailProperties.setProperty("mail.imap.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		javaMailProperties.setProperty("mail.imap.socketFactory.fallback", "false");
		javaMailProperties.setProperty("mail.store.protocol", "imaps");
		//javaMailProperties.setProperty("mail.debug", "true");
		javaMailProperties.setProperty("mail.smtp.timeout", "10000");

		return javaMailProperties;
	}

	@Bean
	ImapIdleChannelAdapter mailAdapter() {

		ImapMailReceiver imapMailReceiver = new ImapMailReceiver(this.imapMailReceiverURL);
		imapMailReceiver.setJavaMailProperties(javaMailProperties());
		imapMailReceiver.setShouldDeleteMessages(false);
		imapMailReceiver.setShouldMarkMessagesAsRead(true);
		imapMailReceiver.afterPropertiesSet();

		ImapIdleChannelAdapter imapIdleChannelAdapter = new ImapIdleChannelAdapter(imapMailReceiver);
		imapIdleChannelAdapter.setAutoStartup(true);
		imapIdleChannelAdapter.setOutputChannel(directChannel());
		
		return imapIdleChannelAdapter;

	}

	@Bean
	public DirectChannel directChannel() {
		return new DirectChannel();
	}

}
