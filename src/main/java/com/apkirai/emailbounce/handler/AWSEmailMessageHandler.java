/**
 * 
 */
package com.apkirai.emailbounce.handler;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.messaging.Message;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * @author tokase
 *
 */
@Service("awsEmailMessageHandler")
public class AWSEmailMessageHandler implements EmailMessageHandler {

	private static final String ORIGINAL_RCPT_TO = "Original-Rcpt-to:";
	private static final String MESSAGE_FEEDBACK_REPORT = "message/feedback-report";
	private static final String CONTENT_TYPE = "Content-Type";
	private static final String MESSAGE_DELIVERY_STATUS = "message/delivery-status";
	private static final String DIAGNOSTIC_CODE = "Diagnostic-Code";
	private static final String FINAL_RECIPIENT = "Final-Recipient:";
	private static final String SOFT_BOUNCE = "4";
	private static final String HARD_BOUNCE = "5";
	private static Log logger = LogFactory.getLog(AWSEmailMessageHandler.class);

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.apkirai.emailbounce.handler.EmailMessageHandler#handleMessage(org.
	 * springframework.messaging.Message)
	 */
	@Async
	public void handleMessage(Message<?> message) {

		String contentType;
		MimeMessage payload = (MimeMessage) message.getPayload();

		try {
			contentType = payload.getContentType();
		} catch (MessagingException e) {
			logger.error("error while getting the content type", e);
			return;
		}

		if (!contentType.startsWith("multipart/report")) {
			logger.info("Ignoring the message as content type is not report:- " + contentType);
			return;
		}

		Multipart mp;
		try {
			mp = (Multipart) payload.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				BodyPart bodyPart = mp.getBodyPart(i);

				String[] contentTypeHeaderValue = bodyPart.getHeader(CONTENT_TYPE);

				boolean anyDeliveryStatusContent = Arrays.stream(contentTypeHeaderValue)
						.anyMatch((str) -> MESSAGE_DELIVERY_STATUS.equalsIgnoreCase(str));
				boolean anyFeedBackContent = Arrays.stream(contentTypeHeaderValue)
						.anyMatch((str) -> MESSAGE_FEEDBACK_REPORT.equalsIgnoreCase(str));

				if (anyDeliveryStatusContent) {
					this.handleDeliveryStatusMessage(bodyPart);
				} else if (anyFeedBackContent) {
					this.handleFeedBackStatusMessage(bodyPart);
				} else {
					continue;
				}

			}

		} catch (IOException | MessagingException e) {
			logger.error("error while get content", e);
		}

	}

	/**
	 * This method handle the Feedback status message. like Mark as spam email
	 * message.
	 * 
	 * @param bodyPart
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void handleFeedBackStatusMessage(BodyPart bodyPart) throws IOException, MessagingException {
		StringWriter writer = new StringWriter();
		IOUtils.copy(bodyPart.getInputStream(), writer);
		String messageContent = writer.toString();		
		this.getFeedBackStatusMessageDetails(messageContent);

	}

	/**
	 * Use this method to get the Feed back message details.
	 * 
	 * @param messageContent
	 * @throws IOException
	 */
	private void getFeedBackStatusMessageDetails(String messageContent) throws IOException {

		@SuppressWarnings("unchecked")
		List<String> lines = IOUtils.readLines(new StringReader(messageContent));

		// Get the final Recipient; the format of the string
		// Original-Rcpt-to: complaint@simulator.amazonses.com

		Optional<String> originalRecipientOptional = lines.stream()
				.filter(finalRecipientLine -> finalRecipientLine.startsWith(ORIGINAL_RCPT_TO)).findFirst();
		if (originalRecipientOptional.isPresent()) {
			String originalRecipient;
			String finalRecipientString = originalRecipientOptional.get();
			String[] splitOriginalRecipient = finalRecipientString.split(" ");
			if (splitOriginalRecipient.length > 1) {
				originalRecipient = splitOriginalRecipient[1] != null ? splitOriginalRecipient[1].trim() : "";
			} else {
				originalRecipient = "<BLANK>";
			}
			logger.info("Original recipient is" + originalRecipient);
		}

	}

	/**
	 * Handle Delivery message from AWS like Hard bounce / Soft Bounce.
	 * 
	 * @param bodyPart
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void handleDeliveryStatusMessage(BodyPart bodyPart) throws IOException, MessagingException {
		StringWriter writer = new StringWriter();
		IOUtils.copy(bodyPart.getInputStream(), writer);
		String messageContent = writer.toString();
		this.getDeliveryStatusMessageDetails(messageContent);
	}

	/***
	 * Use this method to check for the type of the bounce.
	 * 
	 * @param string
	 * @throws IOException
	 */
	private void getDeliveryStatusMessageDetails(String messageContent) throws IOException {
		@SuppressWarnings("unchecked")
		List<String> lines = IOUtils.readLines(new StringReader(messageContent));

		// Get the final Recipient; the format of the string
		// Final-Recipient: rfc822; bounce@simulator.amazonses.com
		Optional<String> finalRecipientOptional = lines.stream()
				.filter(finalRecipientLine -> finalRecipientLine.startsWith(FINAL_RECIPIENT)).findFirst();
		if (finalRecipientOptional.isPresent()) {
			String finalRecipient;
			String finalRecipientString = finalRecipientOptional.get();
			String[] splitFinalRecipient = finalRecipientString.split(";");
			if (splitFinalRecipient.length > 0) {
				finalRecipient = splitFinalRecipient[1];
			} else {
				finalRecipient = "<BLANK>";
			}
			logger.info("final recipient is" + finalRecipient);
		}

		// Get the Diagnostic-Code the format of the string. Diagnostic-Code:
		// smtp; 550 5.1.1 user unknown
		Optional<String> diagnosticCodeOptional = lines.stream()
				.filter(diagnosticCodeLine -> diagnosticCodeLine.startsWith(DIAGNOSTIC_CODE)).findFirst();
		if (diagnosticCodeOptional.isPresent()) {
			String diagnosticCodeString = diagnosticCodeOptional.get();
			String[] diagnoSticCodeStringArray = diagnosticCodeString.split(";");
			boolean isHardbounce = false;
			boolean isBounce = false;
			boolean isSoftBounce = false;
			boolean isGeneralBounce = false;
			if (diagnoSticCodeStringArray.length > 1) {
				String diagnoSticCode = diagnoSticCodeStringArray[1] != null ? diagnoSticCodeStringArray[1].trim() : "";

				if (diagnoSticCode.startsWith(HARD_BOUNCE)) {
					isHardbounce = true;
					isBounce = true;
				} else if (diagnoSticCode.startsWith(SOFT_BOUNCE)) {
					isSoftBounce = true;
					isBounce = true;
				} else {
					isBounce = true;
					isGeneralBounce = true;
				}
			} else {
				isBounce = false;
			}

			logger.info("Bounce information hard bounce :-" + isHardbounce + " is bounce :-" + isBounce
					+ " isSoftBounce :- " + isSoftBounce + " isGeneralBounce:-" + isGeneralBounce);
			;
		}

	}

}
