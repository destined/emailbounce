/**
 * 
 */
package com.apkirai.emailbounce.handler;

import org.springframework.messaging.Message;

/**
 * @author tokase
 *
 */
public interface EmailMessageHandler {

	void handleMessage(Message<?> message);

}
