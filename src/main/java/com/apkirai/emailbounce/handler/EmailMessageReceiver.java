/**
 * 
 */
package com.apkirai.emailbounce.handler;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.MessagingException;
import org.springframework.stereotype.Service;

/**
 * @author tokase
 *
 */
@Service
public class EmailMessageReceiver {

	@Autowired
	private DirectChannel channel;
	
	@Autowired
	@Qualifier("awsEmailMessageHandler")
	private EmailMessageHandler messageHandler; 

	@PostConstruct
	public void handle() {
		channel.subscribe(new MessageHandler() {
			public void handleMessage(Message<?> message) throws MessagingException {
				System.out.println("inside message" + message);
				messageHandler.handleMessage(message);
			}
		});
	}

}
